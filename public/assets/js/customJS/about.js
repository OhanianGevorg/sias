'use strict';
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('input[name=_token]').val()
    }
});

$(function () {

    var sectionHtml = $('<div class="col-md-12 col-xs-12 col-sm-12 section-content-wrapper margin-top">' +
        '<form data-url="section/content/">' +
        '<div class="form-group text-wrapper col-md-6 col-sm-6 col-xs-12">' +
        '<textarea name="armenian" id="about_armenian" class="col-md-12 about-content"></textarea>' +
        '</div>' +
        '<div class="form-group text-wrapper col-md-6 col-sm-6 col-xs-12 margin-left">' +
        '<textarea name="english" id="about_english" class="col-md-12 about-content"></textarea>' +
        '</div>' +
        '<div class="form-group text-wrapper col-md-6 col-sm-6 col-xs-12">' +
        '<textarea name="russian" id="about_russian" class="col-md-12 about-content"></textarea>' +
        '</div>' +
        '<div class="form-group col-md-6 col-sm-6 col-xs-12 images-wrapper margin-left">this should be dropzone </div>' +
        '<div class="form-group col-md-12 col-sm-12 col-xs-12">' +
        '<button class="btn btn-success pull-right save-content" type="button">Save Content</button>' +
        '</div>' +
        '</form>' +
        '</div>');


    $('.add-section').on('click', function (e) {
        e.preventDefault();
        var _this = this;
        var bigParent = $('.' + $(_this).data('content-container'));
        var url = $(_this).data('url');
        var page_id = $(_this).data('page-id');
        console.log(page_id);
        swal({
            title: 'Set Section Name',
            showCancelButton: true,
            input: 'text',
            inputPlaceholder: 'Write Section Name',
            inputAutoTrim: true,
            allowOutsideClick: false,
            preConfirm: function (text) {
                return new Promise(function (resolve, reject) {
                    if (text) {
                        $.ajax({
                            url: url,
                            data: {
                                name: text,
                                page_id: page_id
                            },
                            method: 'POST'
                        }).fail(function (res) {
                            var responseText = '';
                            for (var i in res.responseJSON) {
                                responseText += res.responseJSON[i][0] + '\r';
                            }
                            reject('responseText');
                        }).done(function (res) {
                            if (res.hasOwnProperty('success')) {
                                resolve(res.data);
                            }
                            if (res.hasOwnProperty('error')) {
                                reject(res.error);
                            }
                        });
                    }
                });
            }
        }).then(function (data) {
            var url = $(sectionHtml).find('form').attr('data-url') + data.page_id;
            $(sectionHtml).appendTo($(bigParent));
            $(sectionHtml).find('form').attr('data-url', url);
            $('body').trigger('callTinymce', 'textarea.about-content');
        }, function (dismiss) {
            if (dismiss === 'cancel') {
                swal.close();
            }
        });
    });

    $('body').on('click', '.save-content', function (e) {
        e.preventDefault();
        var _this = this;
        var form = $(_this).closest('form');
        var formData = new FormData();
        var textareas = $(form).find('textarea');
        $.each(textareas,function (index,item) {
            formData.append(item.name,tinyMCE.activeEditor.getContent({format:'html'}));
            console.log(tinyMCE.get(item.name));
        });
    });
});