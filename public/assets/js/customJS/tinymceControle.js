$(function () {
    $('body').on('callTinymce',function (e,selector) {
        e.stopPropagation();
        tinymce.init({
            selector:selector,
            plugins: [
                'advlist autolink lists link image imagetools charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template textcolor colorpicker textpattern codesample toc'
            ],
            menubar: "insert",
            toolbar1: 'undo redo  | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview  | forecolor backcolor emoticons ',
            image_advtab: true,
            file_picker_types: 'image media',
            file_browser_callback: function(field_name, url, type, win) {

                var input,img;

                if(type=='image'){
                    input = document.createElement('input');
                    img = document.createElement('img');
                }

                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                // input.setAttribute('name','inserted_image');

                // Note: In modern browsers input[type="file"] is functional without
                // even adding it to the DOM, but that might not be the case in some older
                // or quirky browsers like IE, so you might want to add it to the DOM
                // just in case, and visually hide it. And do not forget do remove it
                // once you do not need it anymore.

                input.onchange = function() {
                    var file = this.files[0];

                    // Note: Now we need to register the blob in TinyMCEs image blob
                    // registry. In the next release this part hopefully won't be
                    // necessary, as we are looking to handle it internally.
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                    var blobInfo = blobCache.create(id, file);
                    blobCache.add(blobInfo);
                    if(file){
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            img.setAttribute('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                    }
                    tinyMCE.activeEditor.dom.add(tinyMCE.activeEditor.getBody(), img);
                };

                input.click();
            }
        });
    });
});
