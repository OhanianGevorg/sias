<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSectionsTable extends Migration
{
    public function up()
    {
        //
        Schema::create('sections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id');
            $table->string('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sections');
    }
}
