<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Shared\SectionNameRequest;
use App\Models\Page;
use App\Models\Section;
use App\Transformers\PageTransformer;
use App\Transformers\SectionSmallTransformer;
use Illuminate\Support\Facades\Route;

class AboutController extends Controller
{
    //
    public function index()
    {

        $name = explode('.',Route::CurrentRouteName());

        $page = Page::where('name','=',$name[0])->first();

        if(!$page){
            $page = new Page(array('name'=>$name[0]));
            $page->save();
        }
        $page_data = PageTransformer::transform($page);

        return view('admin.pages.about',compact('page_data'));
    }

    public function addSection(SectionNameRequest $request)
    {
       $page = Page::find($request->get('page_id'));

        if(!$page){
            return response()->json([
               'error'=>'This page is unavailable'
            ]);
        }
        try{
            $section = Section::create(array('title'=>$request->get('name'),'page_id'=>$page->getKey()));
            return response()->json([
               'success'=>'Data Created',
                'data'=>SectionSmallTransformer::transform($section)
            ]);
        }catch(\Exception $e){
            return response()->json([
               'error'=>'You have error'
            ]);
        }

    }
}