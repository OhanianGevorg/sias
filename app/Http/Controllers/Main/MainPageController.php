<?php

namespace App\Http\Controllers\main;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MainPageController extends Controller
{
    //
    public function index(){
        return view('default.pages.home.home');
    }
}
