<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware'=>['web']],function(){
    
    Route::group(['namespace'=>'Main'],function(){
        Route::resource('/','MainPageController');
    });

    Route::group(['namespace'=>'Admin'],function(){
        Route::resource('/admin','AdminController');
        Route::resource('/about','AboutController');
        Route::post('/about/addSection','AboutController@addSection');
        Route::resource('/news','NewsController');
        Route::resource('/professions','ProfessionController');
        Route::resource('/team','TeamController');
        Route::resource('/gallery','GalleryController');
    });

});
