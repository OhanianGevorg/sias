<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'pages';
    protected $fillable = [
        'name'
    ];
    //
    private static $pagesArray = [
        'About'=>[
            'route_name'=>'about',
            'icon'=>'glyphicon-home'
        ],
        'News'=>[
            'route_name'=>'news',
            'icon'=>'glyphicon-bell'
        ],
        'Professions'=>[
            'route_name'=>'professions',
            'icon'=>'glyphicon-education'
        ],
        'Our Team'=>[
            'route_name'=>'team',
            'icon'=>'glyphicon-bullhorn'
        ],
        'Gallery'=>[
            'route_name'=>'gallery',
            'icon'=>'glyphicon-camera'
        ]
    ];

    public static function getPagesArray()
    {
        return self::$pagesArray;
    }

    public function sections()
    {
        return $this->hasMany(Section::class, 'page_id');
    }

    public function getSectionImages()
    {
        return $this->hasMany(Section::class, 'page_id')->getResults()->map(function ($relation) {
            return $relation->images();
        });
    }
}
