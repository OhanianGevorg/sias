<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    //
    protected $fillable = [
        'page_id',
        'title'
    ];

    public function images()
    {
        return $this->hasMany(Image::class, 'section_id');
    }

    public function texts()
    {
        return $this->hasMany(SectionText::class, 'section_id');
    }

}
