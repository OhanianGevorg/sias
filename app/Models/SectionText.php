<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SectionText extends Model
{
    //
    protected $fillable =[
        'section_id',
        'text_title',
        'text'
    ];

    public function section()
    {
        return $this->belongsTo(Section::class,'section_id');
    }
}
