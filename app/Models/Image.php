<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $fillable =[
        'section_id',
        'file_name'
    ];
    
    public function section()
    {
        return $this->belongsTo(Section::class,'section_id');
    }
}
