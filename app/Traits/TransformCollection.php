<?php
namespace App\Traits;
use Illuminate\Database\Eloquent\Collection;

/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 1/16/2017
 * Time: 10:05 PM
 */
trait TransformCollection
{
    public static function transformCollection(Collection $models)
    {
        if(!$models){
            return [];
        }
        $results = [];
        foreach ($models as $key=>$model){
            $results[] = self::transform($model);
        }
        return $results;
    }
}