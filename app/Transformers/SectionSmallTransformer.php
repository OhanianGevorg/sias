<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 1/17/2017
 * Time: 9:06 AM
 */

namespace App\Transformers;


use App\Traits\TransformCollection;
use Illuminate\Database\Eloquent\Model;

class SectionSmallTransformer
{

    use TransformCollection;

    public static function transform(Model $model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'page_id' => $model->page_id
        ];
    }
}