<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 1/16/2017
 * Time: 10:16 PM
 */

namespace App\Transformers;


use App\Traits\TransformCollection;
use Illuminate\Database\Eloquent\Model;

class SectionTransformer
{
    use TransformCollection;
    public static function transform(Model $model)
    {
        if (!$model){
            return [];
        }
        return [
            'id'=>$model->getKey(),
            'page_id'=>$model->page_id,
            'title'=>$model->title,
//            'images'=>ImageTransformer::transformCollection($model->images)
//            'section_text'=>SetcionTextTransformer::transform($model->section_text)
        ];
    }
}