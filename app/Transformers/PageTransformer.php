<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 1/16/2017
 * Time: 10:29 PM
 */

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

class PageTransformer
{
    public static function transform(Model $model)
    {
        if(!$model){
            return [];
        }
        return [
            'id'=>$model->getKey(),
            'name'=>$model->name,
            'sections'=>SectionTransformer::transformCollection($model->sections()->get())
        ];
    }
}