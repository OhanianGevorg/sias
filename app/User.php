<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    private $role = [
        'Admin' => 0,
        'Editor'=>1
    ];

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    public function isRole($role)
    {
        return $this->role_id === $this->role[$role];
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
}
