<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentellela Alela! | </title>

    <!-- Bootstrap -->
    <link href="{{URL::asset("assets/vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{URL::asset("assets/vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{URL::asset("assets/vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{URL::asset("assets/vendors/iCheck/skins/all.css")}}" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="{{URL::asset("assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css")}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{URL::asset("assets/vendors/jqvmap/dist/jqvmap.min.css")}}" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="{{URL::asset("assets/build/css/custom.min.css")}}" rel="stylesheet">
    <link href="{{URL::asset("assets/plugins/sweetalert2/sweetalert2.css")}}" rel="stylesheet">
    <link href="{{URL::asset("css/custom.css")}}" rel="stylesheet">
</head>

@yield('head')
@yield('mainContent')
</html>