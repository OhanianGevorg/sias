@extends('admin.layout.main')

@section('head')
    <body class="nav-md">
    <div class="container body">

        <div class="main_container">

            @endsection

            @section('mainContent')
            <div class="col-md-3 left_col">
                @include('admin.parts.sidebar')
            </div>

            @include('admin.parts.top-navigation')

            @yield('content')
                    <!-- footer content -->
            <footer>
                <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>
    @include('admin.parts.scripts')

    </body>

@endsection