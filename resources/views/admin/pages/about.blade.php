@extends('admin.layout-templates.index-template')

@section('content')
    <!-- page content -->
    <div class="right_col main-div" role="main" id="main-admin-page">

        <!-- /top tiles -->

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph">
                    {{csrf_field()}}
                    <div class="row x_title">
                        <div class="col-md-6">
                            <h3>About settings</h3>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 section-wrapper margin-top">
                        <div class="col-md-12 col-sm-12 col-xs-12 add-section-wrapper">
                            <button class="btn btn-small btn-success pull-left add-section margin-top" data-content-container="section-wrapper" data-url="/about/addSection" data-page-id="{{$page_data['id']}}">Add New Section</button>
                        </div>

                        {{--<div class="col-md-12 col-xs-12 col-sm-12 section-content-wrapper margin-top">--}}
                            {{--<form data-url="section/content/id">--}}
                                {{--<div class="form-group text-wrapper col-md-6 col-sm-6 col-xs-12">--}}
                                    {{--<textarea name="section[0][armenian][section_content]" id="about_armenian" class="col-md-12 about-content"></textarea>--}}
                                {{--</div>--}}
                                {{--<div class="form-group text-wrapper col-md-6 col-sm-6 col-xs-12 margin-left">--}}
                                    {{--<textarea name="section[0][english][section_content]" id="about_english" class="col-md-12 about-content"></textarea>--}}
                                {{--</div>--}}
                                {{--<div class="form-group text-wrapper col-md-6 col-sm-6 col-xs-12">--}}
                                    {{--<textarea name="section[0][russian][section_content]" id="about_russian" class="col-md-12 about-content"></textarea>--}}
                                {{--</div>--}}
                                {{--<div class="form-group col-md-6 col-sm-6 col-xs-12 images-wrapper margin-left">--}}
                                    {{--this should be dropzone--}}
                                {{--</div>--}}
                                {{--<div class="form-group col-md-12 col-sm-12 col-xs-12">--}}
                                    {{--<button class="btn btn-success pull-right">Save Content</button>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>
    <!-- /page content -->
@endsection