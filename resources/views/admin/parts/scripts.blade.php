<!-- jQuery -->
<script src="{{URL::asset("assets/vendors/jquery/dist/jquery.min.js")}}"></script>
<!-- Bootstrap -->
<script src="{{URL::asset("assets/vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
<!-- FastClick -->
<script src="{{URL::asset("assets/vendors/fastclick/lib/fastclick.js")}}"></script>
<!-- NProgress -->
<script src="{{URL::asset("assets/vendors/nprogress/nprogress.js")}}"></script>
<!-- Chart.js -->
<script src="{{URL::asset("assets/vendors/Chart.js/dist/Chart.min.js")}}"></script>
<!-- gauge.js -->
<script src="{{URL::asset("assets/vendors/gauge.js/dist/gauge.min.js")}}"></script>
<!-- bootstrap-progressbar -->
<script src="{{URL::asset("assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>
<!-- iCheck -->
<script src="{{URL::asset("assets/vendors/iCheck/icheck.min.js")}}"></script>
<!-- Skycons -->
<script src="{{URL::asset("assets/vendors/skycons/skycons.js")}}"></script>
<!-- Flot -->
<script src="{{URL::asset("assets/vendors/Flot/jquery.flot.js")}}"></script>
<script src="{{URL::asset("assets/vendors/Flot/jquery.flot.pie.js")}}"></script>
<script src="{{URL::asset("assets/vendors/Flot/jquery.flot.time.js")}}"></script>
<script src="{{URL::asset("assets/vendors/Flot/jquery.flot.stack.js")}}"></script>
<script src="{{URL::asset("assets/vendors/Flot/jquery.flot.resize.js")}}"></script>
<!-- Flot plugins -->
<script src="{{URL::asset("assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js")}}"></script>
<script src="{{URL::asset("assets/vendors/flot-spline/js/jquery.flot.spline.min.js")}}"></script>
<script src="{{URL::asset("assets/vendors/flot.curvedlines/curvedLines.js")}}"></script>
<!-- DateJS -->
<script src="{{URL::asset("assets/vendors/DateJS/build/date.js")}}"></script>
<!-- JQVMap -->
<script src="{{URL::asset("assets/vendors/jqvmap/dist/jquery.vmap.js")}}"></script>
<script src="{{URL::asset("assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js")}}"></script>
<script src="{{URL::asset("assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js")}}"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{URL::asset("assets/vendors/moment/src/moment.js")}}"></script>
<script src="{{URL::asset("assets/vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
<!-- Custom Theme Scripts -->
<script src="{{URL::asset("assets/plugins/sweetalert2/sweetalert2.js")}}"></script>
<script src="{{URL::asset("assets/plugins/tinymce/js/tinymce/jquery.tinymce.min.js")}}"></script>
<script src="{{URL::asset("assets/plugins/tinymce/js/tinymce/tinymce.min.js")}}"></script>
<script src="{{URL::asset("assets/js/customJS/tinymceControle.js")}}"></script>
<script src="{{URL::asset("assets/build/js/custom.min.js")}}"></script>
<script src="{{URL::asset("assets/js/customJS/about.js")}}"></script>